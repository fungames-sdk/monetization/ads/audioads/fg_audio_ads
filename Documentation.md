# Audio Ads

## Introduction

Audio Ads can be added to your game in addition of other Ads, in order to improve the revenues without compromising too much the user experience.

## Integration Steps

1) **"Install"** or **"Upload"** FG AudioAds plugin from the FunGames Integration Manager in Unity, or download it from here.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

## Remote Config Settings

AudioAds must be enabled using the “AudioAdsConfig” remote variable. It can be used to target a single AudioAd module or handle backfill feature as follow :

```AudioAdsConfig = 213 (example)```

(here 0,1,2,3, etc.. refers to the config value for each network that we already use: AudioMob=2, Odeeo=1, GadsmeAudio =3 etc.)

## Optimize Impressions using an AudioMixer

The AudioAds modules works in addition with a default AudioAdsMixer in order to optimize Audio Impressions by muting all others Game audios while an ad is playing. To bind your existing AudioSources with the AudioAdsMixer, you just need to change their output to ‘OtherGameAudio’ (see Example 1).

You can also use your own AudioMixerController but make sure you set up 2 snapshots : one for Normal Mode, and one with all your game audios muted (see Example 2).
You can disable this feature by unchecking Mute Other In Game Audio in the FGAudioMixerHandler component of the FGAudioAds prefab.

**Example 1: Change the AudioSource output :**
For each audio you want to integrate with the AudioAdsMixer, add in its output field the
‘OtherGameAudio’ Audio Mixer Group Controller.

![](_source/audioAds_Example1.png)

**Example 2: Use your own AudioMixer :**
You can either use the default AudioAdsMixer in the _Assets > FunGames > Monetization > AudioAds_ folder or create/use a custom one, but make sure you have at least 2 snapshots as follow:

- Normal Mode -> All groups are playing

![](_source/audioAds_Example2_Normal.png)

- Audio Ad Playing -> All other game audios are muted

![](_source/audioAds_Example2_AudioPlaying.png)

If using a custom AudioMixerController you must drag the 2 different snapshots in the
FGAudioMixerHandler component of the FGAudioAds prefab.

![](_source/audioAds_Example2_AudioMixerHandler.png)

## Scripting API

> You can use these functions to play an audio ad :

```csharp
FGAudioAds.PlaySkippableAd(bool withLogo, float skipCountdown);
FGAudioAds.PlayRewardedAd(bool withLogo);
```

> You can also subscribe to these callbacks if you want to perform custom actions when an ad is played or when it completed (to reward users for example) :

```csharp
FGAudioAds.Callbacks.OnSkippableStarted;
FGAudioAds.Callbacks.OnRewardedStarted;
FGAudioAds.Callbacks.OnSkippableCompleted;
FGAudioAds.Callbacks.OnRewardedCompleted;
```

